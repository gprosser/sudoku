#include <iostream>
#include <vector>
#include <ctime>

struct State
{
    int grid[81] = {};
    bool rows[9][9] = {};
    bool cols[9][9] = {};
    bool boxs[3][3][9] = {};
    int next_cell = 0;
};

int main()
{
    // read input and set up initial state
    State initial;

    for (int row = 0; row < 9; ++row)
    {
        for (int col = 0; col < 9; ++col)
        {
            int digit;
            std::cin >> digit;
            if (digit != 0)
            {
                initial.grid[9*row + col] = digit;
                initial.rows[row][digit-1] = true;
                initial.cols[col][digit-1] = true;
                initial.boxs[row/3][col/3][digit-1] = true;
            }
        }
    }

    while (initial.next_cell < 81)
    {
        if (initial.grid[initial.next_cell] == 0)
            break;
        ++initial.next_cell;
    }


    // depth first search of the sudoku state space
    std::vector<State> states;
    states.push_back(initial);

    std::clock_t c_start = std::clock();

    while(!states.empty())
    {
        State state = states.back();
        states.pop_back();

        int cell = state.next_cell;
        int row = cell / 9;
        int col = cell % 9;

        for (int digit = 1; digit <= 9; ++digit)
        {
            // see if each digit could fit
            if (!state.rows[row][digit-1] && !state.cols[col][digit-1] && !state.boxs[row/3][col/3][digit-1])
            {
                // if it fits, put it in and carry on to the next empty cell
                State new_state = state;
                new_state.grid[cell] = digit;
                new_state.rows[row][digit-1] = true;
                new_state.cols[col][digit-1] = true;
                new_state.boxs[row/3][col/3][digit - 1] = true;

                // find the next empty cell
                while (new_state.next_cell < 81)
                {
                    if (new_state.grid[new_state.next_cell] == 0)
                        break;
                    ++new_state.next_cell;
                }

                if (new_state.next_cell < 81)
                {
                    states.push_back(new_state);
                }
                else
                {
                    // solved! print solution and exit
                    std::clock_t c_end = std::clock();
                    std::cout << "solved in " << 1000.f * (c_end - c_start) / CLOCKS_PER_SEC << "ms\n";

                    for (int r = 0; r < 9; ++r)
                    {
                        for (int c = 0; c < 9; ++c)
                        {
                            std::cout << new_state.grid[9*r + c] << " ";
                        }
                        std::cout << "\n";
                    }

                    return 0;
                }
            }
        }
    }

    std::cout << "something went wrong!\n";

    return 0;
}